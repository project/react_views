<?php
/**
 * Created by PhpStorm.
 * User: vb
 * Date: 18/2/19
 * Time: 5:01 PM
 */
/**
 * @file
 * Contains Drupal\react_views\Form\SetViewsForReact.
 */
namespace Drupal\react_views\Form;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\views\Views;

class SetViewsForReact extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'react_views.adminsettings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'set_views_for_react';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('react_views.adminsettings');
    $views = [];
    foreach ( Views:: getEnabledViews() As $key => $value ){
      $views[$key] = $value->label().'       ['.$key.']';
    }
    $form['react-view-name'] = [
      '#type' => 'checkboxes',
      '#title' => $this->t('Select view'),
      '#description' => $this->t('Select the views which will be display as react component'),
      '#default_value' => $config->get('react_views'),
      '#options' => $views
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);
    $selectedViews = [];
    foreach ($form_state->getValue('react-view-name') as $key => $item) {
      if($item != '0'){
        $selectedViews[] = $item;
      }
    }
    $this->config('react_views.adminsettings')
      ->set('react_views', $selectedViews )
      ->save();
  }
}